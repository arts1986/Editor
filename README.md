Editor
====
【安装】
----
```python
    bower install jquery
    git clone git@git.oschina.net:youe/Editor.git
```

【HTML】
----
```html
    <html>
        <head>
            <title>多功能编辑器</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" href="css/pages/index.css"/>   <!-- 插件css -->
        </head>
        <body>

            <div id='edit'></div>

            <script src='lib/jquery.min.js'></script>   <!-- 引入jquery -->
            <script src='js/page/edit.js'></script>     <!-- 引入插件 -->
        </body>
    </html>
```

【JS】
----
```javascript
    var edit=new Editor("edit");    //填入div的id
    edit.create();
```

【基本配置】
----

###1.菜单配置
> 插件有自带的默认配置如无需特殊处理不需要自定义配置
```javascript
    var edit=new Editor("edit");
    edit.config.menus = [
            "source",        //源码
            "bold",        //粗体
            "underline",        //下划线
            "italic",        //斜体
            "strikethrough",        //栅格线
            "eraser",        //清除格式
            "forecolor",        //字体颜色
            "bgcolor",        //背景颜色
            "quote",        //引用
            "fontfamily",        //字体类型
            "fontsize",        //字体大小
            "head",        //标题
            "unorderlist",        //无序列表
            "orderlist",        //有序列表
            "alignleft",        //左对齐
            "aligncenter",        //居中
            "alignright",        //右对齐
            "link",        //超链接
            "unlink",        //撤销 超链接
            "table",        //表格
            "emotion",        //表情
            "img",        //图片
            "video",        //视频
            "location",        //地图
            "insertcode",        //代码
            "undo",        //撤销
            "redo",        //重做
            "fullscreen",        //全屏
    ];
    edit.create();
```

###2.语言配置
> 默认为中文
```javascript
    var edit=new Editor("edit");
    edit.config.lang = Editor.langs["zh-cn"];
    edit.create();
```
```javascript
    var edit=new Editor("edit");
    edit.config.lang = Editor.langs["en"];
    edit.create();
```

###3.编码过滤`javascript`
> 默认为开启过滤
```javascript
    var edit=new Editor("edit");
    edit.config.jsFilter = false;
    edit.create();
```
```javascript
    var edit=new Editor("edit");
    edit.config.jsFilter = true;
    edit.create();
```

【基本功能】
----

###1.创建富文本编辑器
> 初始化
```javascript
    var edit=new Editor("edit");
    edit.create();
```

###2.销毁富文本编辑器
> 调用方法前需要先调用创建功能（下同）
```javascript
    var edit=new Editor("edit");
    edit.create();
    edit.destroy();
```

###3.撤销销毁富文本编辑器
> 撤销销毁富文本编辑器
```javascript
    var edit=new Editor("edit");
    edit.create();
    edit.undestroy();
```

###4.禁用富文本编辑器
> 禁用富文本编辑器
```javascript
    var edit=new Editor("edit");
    edit.create();
    edit.disable();
```

###5.启用富文本编辑器
> 启用富文本编辑器
```javascript
    var edit=new Editor("edit");
    edit.create();
    edit.enable();
```

###6.清空数据
> 清空数据
```javascript
    var edit=new Editor("edit");
    edit.create();
    edit.clear();
```

###7.获取数据html形式
> 获取数据html形式,请打开控制台查看
```javascript
    var edit=new Editor("edit");
    edit.create();
    console.log(edit.$txt.html());
```

###8.获取数据纯文本形式
> 获取数据纯文本形式,请打开控制台查看
```javascript
    var edit=new Editor("edit");
    edit.create();
    console.log(edit.$txt.text());
```

###9.获取数据格式化形式
> 获取数据格式化形式,请打开控制台查看
```javascript
    var edit=new Editor("edit");
    edit.create();
    console.log(edit.$txt.formatText());
```

【图片配置】
----

###1.自定义上传配置参数
> 请注意必填参数
```javascript
    var edit=new Editor("edit");
    //input name 配置（必须）
    edit.config.uploadImgFileName="imageFile";
    //请求地址（必须）
    edit.config.uploadImgUrl="/res/uploadImage.do";
    //json img地址（必须）
    edit.config.jsonImgSrc="data";
    //回调函数（可选）
    edit.config.uploadImgFns={
            //上传时，注：添加图片由自己写 || 不写为插件自带效果
            onload:function(){
                    console.log("onload");
            }
            //上传成功
            onsuccess:function(data){
                    console.log(data);
            }
            //上传失败
            onerror:function(data){
                    console.log(data);
            }
            //上传超时
            ontimeout:function(data){
                    console.log(data);
            }
    };
    edit.create();
```

【字体配置】
----

###1.字号配置
> 有默认字号库如无需特殊处理不需要自定义配置
```javascript
    var edit=new Editor("edit");
    edit.config.fontsizes = {
            1: "12px",
            2: "13px",
            3: "16px",
            4: "18px",
            5: "24px",
            6: "32px",
            7: "48px"
    };
    edit.create();
```

###2.字体配置
> 有默认字体库如无需特殊处理不需要自定义配置
```javascript
    var edit=new Editor("edit");
    edit.config.familys = [
            "宋体", "黑体", "楷体", "微软雅黑",
            "Arial", "Verdana", "Georgia",
            "Times New Roman", "Microsoft JhengHei",
            "Trebuchet MS", "Courier New", "Impact", "Comic Sans MS", "Consolas"
    ];
    edit.create();
```

###3.颜色配置
> 有默认颜色库如无需特殊处理不需要自定义配置
```javascript
    var edit=new Editor("edit");
    edit.config.colors = {
            "#880000": "暗红色",
            "#800080": "紫色",
            "#ff0000": "红色",
            "#ff00ff": "鲜粉色",
            "#000080": "深蓝色",
            "#00ffff": "湖蓝色",
            "#008080": "蓝绿色",
            "#008000": "绿色",
            "#808000": "橄榄色",
            "#00ff00": "浅绿色",
            "#ffcc00": "橙黄色",
            "#808080": "灰色",
            "#c0c0c0": "银色",
            "#000000": "黑色",
            "#ffffff": "白色"
    };
    edit.create();
```

【表情配置】
----

###1.表情包导入
> 有默认表情包如无需特殊处理不需要自定义配置
```javascript
    var edit=new Editor("edit");
    editor.config.emotions = {
            // 支持多组表情
            // 第一组，id叫做 "default"
            "default": {
                    title: "默认",  // 组名称
                    data: "./emotions.data"  // 服务器的一个json文件url，例如官网这里配置的是 http://www.wangeditor.com/wangEditor/test/emotions.data
            },
            // 第二组，id叫做"weibo"
            "weibo": {
                    title: "微博表情",  // 组名称
                    data: [  // data 还可以直接赋值为一个表情包数组
                            // 第一个表情
                            {
                            "icon": "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7a/shenshou_thumb.gif",
                            "value": "[草泥马]"
                            },
                            // 第二个表情
                            {
                            "icon": "http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/60/horse2_thumb.gif",
                            "value": "[神马]"
                            }
                            // 下面还可以继续，第三个、第四个、第N个表情。。。
                    ]
            }
            // 下面还可以继续，第三组、第四组、、、
    };
    edit.create();
```

###2.表情显示icon形式
> 编辑器显示为icon
```javascript
    var edit=new Editor("edit");
    edit.config.emotionsShow = "icon"
    edit.create();
```

###3.表情显示文本形式
> 编辑器显示为文本
```javascript
    var edit=new Editor("edit");
    edit.config.emotionsShow = "value"
    edit.create();
```

【地图配置】
----

###1.百度地图key配置
> key配置，如没有请[点击申请](http://lbsyun.baidu.com/apiconsole/key)
```javascript
    var edit=new Editor("edit");
    edit.config.mapAk = "TVhjYjq1ICT2qqL5LdS8mwas";
    edit.create();
```