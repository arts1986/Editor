
    //设置中文
    $('#cn').on('click',function(){
        $('textarea').val(
            editorInfo(
                textareaAfterEnter(editStart())+
                'edit.config.lang = Editor.langs["zh-cn"];'+
                textareaBeforeEnter(editCreat())
            )
        );
    });

    //设置英文
    $('#en').on('click',function(){
        $('textarea').val(
            editorInfo(
                textareaAfterEnter(editStart())+
                'edit.config.lang = Editor.langs["en"];'+
                textareaBeforeEnter(editCreat())
            )
        );
    });

    // 编辑源码时，不过滤 javascript
    $('#enjsFilter').on('click',function(){
        $('textarea').val(
            editorInfo(
                textareaAfterEnter(editStart())+
                'edit.config.jsFilter = false;'+
                textareaBeforeEnter(editCreat())
            )
        );
    });

    // 编辑源码时，过滤 javascript
    $('#disjsFilter').on('click',function(){
        $('textarea').val(
            editorInfo(
                textareaAfterEnter(editStart())+
                'edit.config.jsFilter = true;'+
                textareaBeforeEnter(editCreat())
            )
        );
    });

    //菜单配置
    $('#menus').on('click',function(){
        $('textarea').val(
            textareaAfterEnter('示例')+
            editorInfo(
                textareaAfterEnter(editStart())+
                'edit.config.menus = ['+
                    textareaBeforeEnter(textareaInline1('"source",')+textareaInline1('//源码'))+
                    textareaBeforeEnter(textareaInline1('"bold",')+textareaInline1('//粗体'))+
                    textareaBeforeEnter(textareaInline1('"underline",')+textareaInline1('//下划线'))+
                    textareaBeforeEnter(textareaInline1('"italic",')+textareaInline1('//斜体'))+
                    textareaBeforeEnter(textareaInline1('"strikethrough",')+textareaInline1('//栅格线'))+
                    textareaBeforeEnter(textareaInline1('"eraser",')+textareaInline1('//清除格式'))+
                    textareaBeforeEnter(textareaInline1('"forecolor",')+textareaInline1('//字体颜色'))+
                    textareaBeforeEnter(textareaInline1('"bgcolor",')+textareaInline1('//背景颜色'))+
                    textareaBeforeEnter(textareaInline1('"quote",')+textareaInline1('//引用'))+
                    textareaBeforeEnter(textareaInline1('"fontfamily",')+textareaInline1('//字体类型'))+
                    textareaBeforeEnter(textareaInline1('"fontsize",')+textareaInline1('//字体大小'))+
                    textareaBeforeEnter(textareaInline1('"head",')+textareaInline1('//标题'))+
                    textareaBeforeEnter(textareaInline1('"unorderlist",')+textareaInline1('//无序列表'))+
                    textareaBeforeEnter(textareaInline1('"orderlist",')+textareaInline1('//有序列表'))+
                    textareaBeforeEnter(textareaInline1('"alignleft",')+textareaInline1('//左对齐'))+
                    textareaBeforeEnter(textareaInline1('"aligncenter",')+textareaInline1('//居中'))+
                    textareaBeforeEnter(textareaInline1('"alignright",')+textareaInline1('//右对齐'))+
                    textareaBeforeEnter(textareaInline1('"link",')+textareaInline1('//超链接'))+
                    textareaBeforeEnter(textareaInline1('"unlink",')+textareaInline1('//撤销 超链接'))+
                    textareaBeforeEnter(textareaInline1('"table",')+textareaInline1('//表格'))+
                    textareaBeforeEnter(textareaInline1('"emotion",')+textareaInline1('//表情'))+
                    textareaBeforeEnter(textareaInline1('"img",')+textareaInline1('//图片'))+
                    textareaBeforeEnter(textareaInline1('"video",')+textareaInline1('//视频'))+
                    textareaBeforeEnter(textareaInline1('"location",')+textareaInline1('//地图'))+
                    textareaBeforeEnter(textareaInline1('"insertcode",')+textareaInline1('//代码'))+
                    textareaBeforeEnter(textareaInline1('"undo",')+textareaInline1('//撤销'))+
                    textareaBeforeEnter(textareaInline1('"redo",')+textareaInline1('//重做'))+
                    textareaBeforeEnter(textareaInline1('"fullscreen",')+textareaInline1('//全屏'))+
                textareaBeforeEnter('];')+
                textareaBeforeEnter(editCreat())
            )
        )
    });

    $('#happy').on('click',function(){
        $('textarea').val(editStart()+ 'edit.config.emotions={'+
            '\r\n'+'        "new":{'+
            '\r\n'+'                title:"111",' +
            '\r\n'+'                data:['+
            '\r\n'+'                        {'+
            '\r\n'+'                                icon:"http://img.t.sinajs.cn/t35/style/images/common/face/ext/normal/7a/shenshou_thumb.gif",'+
            '\r\n'+'                                value:"[草泥马]"'+
            '\r\n'+'                        }'+
            '\r\n'+'                ]'+
            '\r\n'+'}'+editCreat()
        )
    });


    // 全屏时的 z-index
    //edit.config.zindex = 10000;

