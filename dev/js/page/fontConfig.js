$('#fontsize').on('click',function(){
    $('textarea').val(
        textareaAfterEnter('示例')+
        editorInfo(
            textareaAfterEnter(editStart())+
            'edit.config.fontsizes = {'+
            textareaBeforeEnter(textareaInline1('1: "12px",'))+
            textareaBeforeEnter(textareaInline1('2: "13px",'))+
            textareaBeforeEnter(textareaInline1('3: "16px",'))+
            textareaBeforeEnter(textareaInline1('4: "18px",'))+
            textareaBeforeEnter(textareaInline1('5: "24px",'))+
            textareaBeforeEnter(textareaInline1('6: "32px",'))+
            textareaBeforeEnter(textareaInline1('7: "48px"'))+
            textareaBeforeEnter('};')+
            textareaBeforeEnter(editCreat())
        )
    );
});
$('#fontfamily').on('click',function(){
    $('textarea').val(
        textareaAfterEnter('示例')+
        editorInfo(
            textareaAfterEnter(editStart())+
            'edit.config.familys = ['+
            textareaBeforeEnter(textareaInline1('"宋体", "黑体", "楷体", "微软雅黑",'))+
            textareaBeforeEnter(textareaInline1('"Arial", "Verdana", "Georgia",'))+
            textareaBeforeEnter(textareaInline1('"Times New Roman", "Microsoft JhengHei",'))+
            textareaBeforeEnter(textareaInline1('"Trebuchet MS", "Courier New", "Impact", "Comic Sans MS", "Consolas"'))+
            textareaBeforeEnter('];')+
            textareaBeforeEnter(editCreat())
        )
    );
});
$('#fontcolor').on('click',function(){
    $('textarea').val(
        textareaAfterEnter('示例')+
        editorInfo(
            textareaAfterEnter(editStart())+
            'edit.config.colors = {'+
            textareaBeforeEnter(textareaInline1('"#880000": "暗红色",'))+
            textareaBeforeEnter(textareaInline1('"#800080": "紫色",'))+
            textareaBeforeEnter(textareaInline1('"#ff0000": "红色",'))+
            textareaBeforeEnter(textareaInline1('"#ff00ff": "鲜粉色",'))+
            textareaBeforeEnter(textareaInline1('"#000080": "深蓝色",'))+
            textareaBeforeEnter(textareaInline1('"#00ffff": "湖蓝色",'))+
            textareaBeforeEnter(textareaInline1('"#008080": "蓝绿色",'))+
            textareaBeforeEnter(textareaInline1('"#008000": "绿色",'))+
            textareaBeforeEnter(textareaInline1('"#808000": "橄榄色",'))+
            textareaBeforeEnter(textareaInline1('"#00ff00": "浅绿色",'))+
            textareaBeforeEnter(textareaInline1('"#ffcc00": "橙黄色",'))+
            textareaBeforeEnter(textareaInline1('"#808080": "灰色",'))+
            textareaBeforeEnter(textareaInline1('"#c0c0c0": "银色",'))+
            textareaBeforeEnter(textareaInline1('"#000000": "黑色",'))+
            textareaBeforeEnter(textareaInline1('"#ffffff": "白色"'))+
            textareaBeforeEnter('};')+
            textareaBeforeEnter(editCreat())
        )
    );
});