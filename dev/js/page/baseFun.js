
// 禁用编辑器
$('#disable').on('click',function(){
    edit.disable();
    $('textarea').val(
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'edit.disable();'
        )
    );
});

// 启用编辑器
$('#enable').on('click',function(){
    edit.enable();
    $('textarea').val(
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'edit.enable();'
        )
    );
});

// 撤销 销毁编辑器
$('#undestroy').on('click',function(){
    edit.undestroy();
    $('textarea').val(
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'edit.undestroy();'
        )
    );
});

// 销毁编辑器
$('#destroy').on('click',function(){
    edit.destroy();
    $('textarea').val(
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'edit.destroy();'
        )
    );
});

// 清空数据
$('#clear').on('click',function(){
    edit.clear();
    $('textarea').val(
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'edit.clear();'
        )
    );
});

// 获取编辑器区域完整html代码
$('#html').on('click',function(){
    $('textarea').val(
        textareaAfterEnter('结果在console控制台')+
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'console.log(edit.$txt.html());'
        )
    );
    console.log(edit.$txt.html());
});

// 获取编辑器纯文本内容
$('#text').on('click',function(){
    $('textarea').val(
        textareaAfterEnter('结果在console控制台')+
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'console.log(edit.$txt.text());'
        )
    );
    console.log(edit.$txt.text());
});

// 获取格式化后的纯文本
$('#formatText').on('click',function(){
    $('textarea').val(
        textareaAfterEnter('结果在console控制台')+
        editorInfo(
            textareaAfterEnter(editStart())+
            textareaAfterEnter(editCreat())+
            'console.log(edit.$txt.formatText());'
        )
    );
    console.log(edit.$txt.formatText());
});