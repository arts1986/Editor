Editor.config.printLog = true;
var edit=new Editor('edit');
edit.config.uploadImgUrl='/upload';
edit.create();

function textareaBeforeEnter(string){
    return '\r\n'+string;
}

function textareaAfterEnter(string){
    return string+'\r\n';
}

function textareaInline1(string){
    return '        '+string;
}

function textareaInline2(string){
    return '                '+string;
}

function textareaInline3(string){
    return '                        '+string;
}

function editorInfo(string){
    return '使用如下代码即可：'+textareaBeforeEnter(string);
}

function editStart(){
    return 'var edit=new Editor("edit");';
}
function editCreat(){
    return 'edit.create();';
}
